class HelperLoading {
  // Author: Jared Goodwin
  // showLoading() - Display loading wheel.
  // removeLoading() - Remove loading wheel.
  // Requires ECMAScript 6 (any modern browser).
  static showLoading() {
    if (document.getElementById('divLoadingFrame') != null) {
      return;
    }
    var style = document.createElement('style');
    style.id = 'styleLoadingWindow';
    style.innerHTML = `
            .loading-frame {
                position: fixed;
                background-color: rgba(0, 0, 0, 0.8);
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                z-index: 4;
            }

            .loading-track {
                height: 50px;
                display: inline-block;
                position: absolute;
                top: calc(50% - 50px);
                left: 50%;
            }

            .loading-dot {
                height: 5px;
                width: 5px;
                background-color: white;
                border-radius: 100%;
                opacity: 0;
            }

            .loading-dot-animated {
                animation-name: loading-dot-animated;
                animation-direction: alternate;
                animation-duration: .75s;
                animation-iteration-count: infinite;
                animation-timing-function: ease-in-out;
            }

            @keyframes loading-dot-animated {
                from {
                    opacity: 0;
                }

                to {
                    opacity: 1;
                }
            }
        `;
    document.body.appendChild(style);
    var frame = document.createElement('div');
    frame.id = 'divLoadingFrame';
    frame.classList.add('loading-frame');
    for (var i = 0; i < 10; i++) {
      var track = document.createElement('div');
      track.classList.add('loading-track');
      var dot = document.createElement('div');
      dot.classList.add('loading-dot');
      track.style.transform = 'rotate(' + String(i * 36) + 'deg)';
      track.appendChild(dot);
      frame.appendChild(track);
    }
    document.body.appendChild(frame);
    var wait = 0;
    var dots = document.getElementsByClassName('loading-dot');
    for (var i = 0; i < dots.length; i++) {
      window.setTimeout(
        function(dot) {
          dot.classList.add('loading-dot-animated');
        },
        wait,
        dots[i]
      );
      wait += 150;
    }
  }
  static removeLoading() {
    document.body.removeChild(document.getElementById('divLoadingFrame'));
    document.body.removeChild(document.getElementById('styleLoadingWindow'));
  }
}

class DriveTable {
  constructor(
    apiKey,
    sheeId,
    tableOrder = [
      [
        0,
        'asc'
      ]
    ],
    pageLength = 10,
    baseUrl = 'https://sheets.googleapis.com/v4/spreadsheets/',
    hiddenColumns = [
      '_',
      'Informazioni cronologiche',
      'Indirizzo email',
      'Timestamp',
      'Email address'
    ],
    imgSize = 60
  ) {
    this.baseUrl = baseUrl;
    this.tableOrder = tableOrder; // default order -> v. https://datatables.net/examples/basic_init/table_sorting.html
    this.pageLength = pageLength; // row per page (default) v. https://datatables.net/reference/option/pageLength
    this.apiKey = apiKey;
    this.sheetId = sheeId;
    this.firstSheetName = '';
    this._hiddenColumnsStr = hiddenColumns;
    this.imgSize = imgSize;
    // remove old elements
    if (document.getElementById('title') != null) {
      document.getElementById('title').remove();
    }
    if (document.getElementById('sheets') != null) {
      document.getElementById('sheets').remove();
    }
    if (document.getElementById('tData') != null) {
      document.getElementById('tData').remove();
    }

    var style = document.createElement('style');
    style.id = 'styleTable';
    style.innerHTML = `
                        .customLink {
                          color: blue;
                          font-weight: bold;
                          text-decoration: none;
                        }

                        .customLink:hover {
                            color: blue;
                            font-weight: bold;
                            text-decoration: underline;
                        }

                        .imgThumb {
                            max-width: ${this.imgSize - 4}px;
                            max-height: ${this.imgSize - 4}px;
                            height: auto;
                            width: auto;
                            margin: auto;
                            
                        }

                        #photoContainer {
                          width: ${this.imgSize}px;
                          height: ${this.imgSize}px;
                          background-color: white;
                          display: table-cell;
                          text-align: center;
                          vertical-align: middle;
                        }
                      `;
    document.body.appendChild(style);
    var tableContainer;
    var newTableContainer = false;
    if ((tableContainer = document.getElementById('DTtableContainer')) == null) {
      tableContainer = document.createElement('div');
      tableContainer.id = 'DTtableContainer';
      tableContainer.classList = 'container text-center';
      newTableContainer = true;
    }

    tableContainer.innerHTML = `
                                  <div class="page-header" id="DTtitle">
                                      <h4>Loading table...</h4>
                                  </div>
                                  <hr />
                                  <div class="btn-group btn-group-toggle" style="margin-bottom: 20px;" data-toggle="buttons" id="DTsheets">
                                  </div>
                                  <!-- Table  -->
                                  <table class="table table-striped table-sm" cellspacing="0" id="DTtData" style="width:100%">
                                      <caption id="caption"></caption>
                                  </table>
                              `;
    if (newTableContainer) {
      document.body.appendChild(tableContainer);
    }
    HelperLoading.showLoading();
    this.getSheetProperties()
      .then((result) => {
        document.getElementById('DTtitle').innerHTML = '<h4>' + result.title + '</h4>';

        this.firstSheetName = this.showSheetsButtons(result);
        this.showSheet(this.firstSheetName, true, result.sheets.length == 1);
      })
      .catch((error) => {
        alert(error);
        HelperLoading.removeLoading();
      });
  }

  showSheet(sheetName, init = false, singleSheet = false) {
    HelperLoading.showLoading();
    this.getValues(sheetName)
      .then((result) => {
        let tableData = this.combineData(result.formulaValues, result.plainValues);
        if (init) {
          var showOptions = !singleSheet || tableData.tbody.length > pageLength;
          $(document).ready(function() {
            window.gTable = $('#DTtData').DataTable({
              data       : tableData.tbody,
              columns    : tableData.thead,
              paging     : showOptions,
              searching  : showOptions,
              info       : showOptions,
              order      : tableOrder,
              pageLength : pageLength
            });
            // console.log(tableData);
          });
        } else {
          window.gTable.clear().rows.add(tableData.tbody).draw();
        }
        HelperLoading.removeLoading();
      })
      .catch((error) => {
        alert(error);
        HelperLoading.removeLoading();
      });
  }

  async getSheetProperties() {
    let url = this.baseUrl + this.sheetId;
    let queryStr;
    let data;
    let vro;
    let formulaValues, plainValues;
    let sheetTitle;
    let sheets = [];
    let i;

    try {
      // get sheet properties
      queryStr = url + '?key=' + this.apiKey;
      data = await $.getJSON(queryStr);
      sheetTitle = data.properties.title;

      for (i = 0; i < data.sheets.length; i++) {
        sheets.push(data.sheets[i].properties.title);
      }
      return { title: sheetTitle, sheets: sheets };
    } catch (error) {
      throw error;
    }
  }

  async getValues(sheetName) {
    let url = this.baseUrl + this.sheetId;
    let queryStr;
    let data;
    let vro;
    let formulaValues, plainValues;
    let sheetTitle;
    // let sheets = [];
    let i;

    try {
      // celle con formule
      vro = 'FORMULA'; // FORMATTED_VALUE || UNFORMATTED_VALUE || FORMULA
      queryStr = url + '/values/' + sheetName + '?key=' + this.apiKey + '&valueRenderOption=' + vro + '&dateTimeRenderOption=FORMATTED_STRING';
      data = await $.getJSON(queryStr);
      formulaValues = data.values;
      // celle con valori finali
      vro = 'FORMATTED_VALUE';
      queryStr = url + '/values/' + sheetName + '?key=' + this.apiKey + '&valueRenderOption=' + vro + '&dateTimeRenderOption=FORMATTED_STRING';
      data = await $.getJSON(queryStr);
      plainValues = data.values;
      return { formulaValues: formulaValues, plainValues: plainValues };
    } catch (error) {
      alert(error);
    }
  }

  showSheetsButtons(data) {
    let button = '';
    let i;
    let sheetTitle = data.sheetTitle;
    let sheets = data.sheets;
    let row = '';

    document.getElementById('DTsheets').innerHTML = '';

    if (sheets.length == 1) {
      // solo un foglio
      return sheets[0];
    }

    for (i = 0; i < sheets.length; i++) {
      row = '';
      row += '<label class="btn btn-warning btn-sm ' + (i == 0 ? 'active' : '') + '">';
      row +=
        '<input type="radio" name="sheets" id="option' +
        i +
        '" autocomplete="off" ' +
        (i == 0 ? 'checked' : '') +
        ' onChange="driveTable.showSheet(\'' +
        sheets[i] +
        '\')">' +
        sheets[i];
      row += '</label>';
      document.getElementById('DTsheets').innerHTML += row;
    }
    return sheets[0];
  }

  combineData(formulaValues, plainValues) {
    let columns = [];
    let _hiddenColumns = [];
    let dataSet = [];
    let row = [];
    let ncolumns, r, c;
    let link, value, cell;

    if (formulaValues != null && formulaValues.length > 0) {
      ncolumns = formulaValues[0].length; // numero di colonne
    } else {
      return { thead: columns, tbody: dataSet };
    }

    for (c = 0; c < ncolumns; c++) {
      // thead
      cell = formulaValues[0][c];

      if (
        this._hiddenColumnsStr.findIndex(function startsWith(element) {
          return cell.indexOf(element) == 0;
        }) >= 0
      ) {
        _hiddenColumns.push(c);
        continue;
      }

      if (cell != null) {
        columns.push({ title: cell });
      } else {
        columns.push({ title: '' });
      }
    }

    for (r = 1; r < formulaValues.length; r++) {
      // tutte le righe
      row = [];
      for (c = 0; c < ncolumns; c++) {
        // tutte le colonne
        if (_hiddenColumns.indexOf(c) >= 0) {
          continue;
        }
        cell = formulaValues[r][c];
        if (cell != null) {
          if (cell.indexOf('=') == 0) {
            // il campo contiene una formula
            if (cell.toUpperCase().indexOf('=HYPERLINK') == 0) {
              // il campo contiene un link
              //parts = str.split('"');
              link = cell.split('"')[1]; // es. =HYPERLINK("https://sngroup.oapd.inaf.it/group/pasto.html","Visita")
              //                                _____0_____ _____________________1_______________________ 2 ___3__
              //                                =HYPERLINK( https://sngroup.oapd.inaf.it/group/pasto.html , Visita
              // link = parts[1]; label = parts[3] || "Visita";
              // link = cell.slice('=HYPERLINK'.length + 1, -1).split(';')[0]; // OLD method
              value = plainValues[r][c];
              cell = '<a href=' + link + ' class="customLink" style="color: blue;" target="_blank">' + value + '</a>';
            } else if (cell.toUpperCase().indexOf('=IMAGE') == 0) {
              // link = cell.slice('=IMAGE'.length + 1, -1).split(';')[0];
              link = cell.split('"')[1];
              cell = '<div id="photoContainer" class="border rounded"><img src=' + link + ' class="imgThumb"></div>'; // link format (2024): https://drive.google.com/thumbnail?id={ID}&sz=w1000
            } else {
              // altra formula
              cell = plainValues[r][c];
            }
          }
          row.push(cell);
        } else {
          row.push('');
        }
      }
      dataSet.push(row);
    }
    return { thead: columns, tbody: dataSet };
  }
  /*
  // Author: Jared Goodwin
  // showLoading() - Display loading wheel.
  // removeLoading() - Remove loading wheel.
  // Requires ECMAScript 6 (any modern browser).
  static showLoading() {
    if (document.getElementById('divLoadingFrame') != null) {
      return;
    }
    var style = document.createElement('style');
    style.id = 'styleLoadingWindow';
    style.innerHTML = `
            .loading-frame {
                position: fixed;
                background-color: rgba(0, 0, 0, 0.8);
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                z-index: 4;
            }

            .loading-track {
                height: 50px;
                display: inline-block;
                position: absolute;
                top: calc(50% - 50px);
                left: 50%;
            }

            .loading-dot {
                height: 5px;
                width: 5px;
                background-color: white;
                border-radius: 100%;
                opacity: 0;
            }

            .loading-dot-animated {
                animation-name: loading-dot-animated;
                animation-direction: alternate;
                animation-duration: .75s;
                animation-iteration-count: infinite;
                animation-timing-function: ease-in-out;
            }

            @keyframes loading-dot-animated {
                from {
                    opacity: 0;
                }

                to {
                    opacity: 1;
                }
            }
        `;
    document.body.appendChild(style);
    var frame = document.createElement('div');
    frame.id = 'divLoadingFrame';
    frame.classList.add('loading-frame');
    for (var i = 0; i < 10; i++) {
      var track = document.createElement('div');
      track.classList.add('loading-track');
      var dot = document.createElement('div');
      dot.classList.add('loading-dot');
      track.style.transform = 'rotate(' + String(i * 36) + 'deg)';
      track.appendChild(dot);
      frame.appendChild(track);
    }
    document.body.appendChild(frame);
    var wait = 0;
    var dots = document.getElementsByClassName('loading-dot');
    for (var i = 0; i < dots.length; i++) {
      window.setTimeout(
        function(dot) {
          dot.classList.add('loading-dot-animated');
        },
        wait,
        dots[i]
      );
      wait += 150;
    }
  }
  static removeLoading() {
    document.body.removeChild(document.getElementById('divLoadingFrame'));
    document.body.removeChild(document.getElementById('styleLoadingWindow'));
  }
*/
}
